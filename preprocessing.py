import numpy as np
import pandas as pd
import conf
import gc
import os


def get_processed_eye_movement_data(ws):
    """
    解析した視線データをそれぞれマージし、分類器に渡していくための形に整形する

    Parameters
    -------------------------------------
    ws: int
        ウインドウサイズ

    Returns
    -------------------------------------
    features: pandas.DataFrame
        特徴量データフレーム
    participants: pandas.Series
        引き延ばした（データフレームに合わせた）被験者名
    task: pandas.Series
        引き延ばした（データフレームに合わせた）タスクカテゴリ
    merged_trait_labels: pandas.Series
        引き延ばした（データフレームに合わせた）パーソナリティラベル
    feature_columns: pandas.Series
        特徴量名
    """

    if not os.path.exists(os.path.join(os.getcwd(), 'DataFrame/dataframe_{}.csv'.format(ws))):
        features_F = pd.read_csv(os.path.join(os.getcwd(), 'Features/features_F/merged_features_{}.csv'.format(ws)))
        ids_F = pd.read_csv(os.path.join(os.getcwd(), 'Features/features_F/merged_ids_{}.csv'.format(ws)), usecols=[0])

        features_I = pd.read_csv(os.path.join(os.getcwd(), 'Features/features_I/merged_features_{}.csv'.format(ws)))
        ids_I = pd.read_csv(os.path.join(os.getcwd(), 'Features/features_I/merged_ids_{}.csv'.format(ws)), usecols=[0])

        features_E = pd.read_csv(os.path.join(os.getcwd(), 'Features/features_E/merged_features_{}.csv'.format(ws)))
        ids_E = pd.read_csv(os.path.join(os.getcwd(), 'Features/features_E/merged_ids_{}.csv'.format(ws)), usecols=[0])

        traits = pd.read_csv('personality_traits/personality/binned_personality.csv')

        # 各タスクごとのラベルをfeaturesデータフレームに追加
        for f, t in zip([features_F, features_I, features_E], ['F', 'I', 'E']):
            f['task'] = t

        # 被験者名をfeaturesデータフレームに統合
        for f, p in zip([features_F, features_I, features_E], [ids_F['Participant ID'], ids_I['Participant ID'], ids_E['Participant ID']]):
            f['participant'] = p

        df = pd.concat([features_F, features_I, features_E])

        # DataFrameを保存
        os.makedirs(os.path.join(os.getcwd(), 'DataFrame'), exist_ok=True)
        df.to_csv('DataFrame/dataframe_{}.csv'.format(ws), header=True, index=False)

        # 元ファイルのデータをメモリから削除（メモリを軽くするため）
        del features_F, features_I, features_E, ids_F, ids_I, ids_E
        gc.collect()

    else:
        df = pd.read_csv(os.path.join(os.getcwd(), 'DataFrame/dataframe_{}.csv'.format(ws)))

    drop_threshold = df.shape[0] * 0.5    # threshold%以上のNaNを持つ列を削除
    drop_columns = df.loc[:, df.isnull().sum() > drop_threshold].columns

    df = df.drop(drop_columns, axis=1)  #

    # 各タスクにおいて, 50%以上欠損値を持つ被験者を削除
    threshold = 0.5  # ここで%を変えられる

    index = []
    for c in df.loc[:, df.isnull().sum() > 0].columns:
        for p in df['participant'].unique():
            for t in df['task'].unique():
                ex_df = df[(df['participant'] == p) & (df['task'] == t)]
                nan_rows = np.where(np.isnan(ex_df[c]))[0]
                if (len(nan_rows) / ex_df.shape[0]) >= threshold:
                    index.extend(ex_df[np.isnan(ex_df[c])].index)
    index = set(index)

    for idx in index:
        df = df.drop(index=idx, axis=0)

    # 残った欠損値は被験者ごとの平均で埋める
    for c in df.loc[:, df.isnull().sum() > 0].columns:
        for p in df['participant'].unique():
            for t in df['task'].unique():
                average = df[(df['participant'] == p) & (df['task'] == t)][c].mean()
                df.loc[df[(df['participant'] == p) & (df['task'] == t)].index, c] = df.loc[df[(df['participant'] == p) & (df['task'] == t)].index, c].fillna(average)

    # trait_labels
    raw_trait_data = pd.read_csv('personality_traits/personality/personality_sex_age.csv')
    raw_trait_data = raw_trait_data[raw_trait_data['Age'] != 1]  # いらない列を削除（Ageが1以外の被験者だけ使う）
    binned_trait_data = pd.read_csv('personality_traits/personality/binned_personality.csv')

    # タスク名を0,1,2のカテゴリ変数に
    df.loc[df['task'] == 'F', 'task'] = 0
    df.loc[df['task'] == 'I', 'task'] = 1
    df.loc[df['task'] == 'E', 'task'] = 2
    df['task'] = df['task'].astype('int64')

    # BIG5質問紙に答えていない人を削除
    dropcolumns = conf.big5
    df = pd.merge(df, binned_trait_data, on='participant', how='left')
    df.dropna(subset=conf.big5, inplace=True)
    df.drop(dropcolumns, axis=1, inplace=True)

    features = df.drop(['participant', 'task'], axis=1).copy().values
    task = df['task']
    merged_trait_labels = pd.merge(df['participant'], binned_trait_data, on="participant", how="left")
    participants = merged_trait_labels['participant']
    merged_trait_labels.drop('participant', axis=1, inplace=True)

    feature_columns = df.drop(['task', 'participant'], axis=1).columns

    return features, participants, task, merged_trait_labels, feature_columns
