# 情報探索時の視線行動からユーザ属性を推定する
***
視線行動のみからユーザーの属性（情報探索行動およびBIG5因子）を推定する研究で使用した分析ファイル

Python3で書かれてます。詳細はrequirements.txtを参照してください