import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
import csv
import predict_personality  # 自作
from sklearn.ensemble import RandomForestClassifier
import preprocessing  # 自作
import conf


def draw_feature_importances(results_dict, name, ws):
    """
    特徴量重要度をプロット

    Parameters
    ---------------------
    results_dict: dict
        クロスバリデーションの結果を格納した辞書
        cf.     {
                'scores': <list>, 'stratified dummy': <list>, 'frequent dummy': <list>, 'feature importances': <list>, 'feature names': <list>
        }
    name: str
        格納するフォルダ名。 figuresフォルダ以下にnameフォルダが作成される
    ws: int
        ウィンドウサイズ
    """
    dirname = 'figures/{}/'.format(name)

    top = 30  # 上から30個のみグラフ化

    copied_feature_importances = results_dict['feature importances'].copy()
    names_of_best_features = []

    for i in range(top):  # 重要度順に並べ替えた特徴量名のリストを作る
        m = np.argmax(copied_feature_importances)  # 重要度の最大値のインデックスを取得
        names_of_best_features.append(results_dict['feature names'][m])
        copied_feature_importances[m] = -1

    fig, ax = plt.subplots()
    ax.barh(np.arange(len(names_of_best_features)), sorted(results_dict['feature importances'], reverse=True)[:top])
    ax.set_yticks(np.arange(top))
    ax.set_yticklabels(names_of_best_features)
    plt.title('Feature Importance')
    plt.tight_layout()
    plt.savefig(dirname + 'ws_{}_feature_importance.png'.format(ws))
    plt.close(fig=fig)


def draw_each_prediction_in_cv_result(results_dict, ws, name):
    """
    予測結果（F1スコア）の描画

    Parameters
    -----------
    results_dict: dict
        クロスバリデーションの結果を格納した辞書
        cf.     {
                'scores': <list>, 'stratified dummy': <list>, 'frequent dummy': <list>, 'feature importances': <list>, 'feature names': <list>
                }
    name: str
        figuresフォルダ以下にnameフォルダが作成される
    ws: int
        ウインドウサイズ
    """
    dirname = 'figures/{}/'.format(name)
    os.makedirs(dirname, exist_ok=True)
    axis_x_pred = np.arange(1, len(results_dict['scores'])+1, 1)
    axis_x_dummy_1 = np.arange(1.3, len(results_dict['stratified dummy'])+1, 1)
    axis_x_dummy_2 = np.arange(1.6, len(results_dict['frequent dummy'])+1, 1)

    fig, ax = plt.subplots()
    ax.bar(
        axis_x_pred,
        results_dict['scores'],
        label='classifier(average: {:.4f})'.format(np.mean(results_dict['scores'])),
        align='center',
        color='orange',
        width=0.3
    )
    ax.bar(
        axis_x_dummy_1,
        results_dict['stratified dummy'],
        label='stratified(average: {:.4f})'.format(np.mean(results_dict['stratified dummy'])),
        align='center',
        color='red',
        width=0.3
    )
    ax.bar(
        axis_x_dummy_2,
        results_dict['frequent dummy'],
        label='most frequent(average: {:.4f})'.format(np.mean(results_dict['frequent dummy'])),
        align='center',
        color='blue',
        width=0.3
    )
    plt.xticks(np.arange(1.3, conf.outer_n_splits+1, 1.0), axis_x_pred)
    plt.xlabel('Fold')
    plt.ylabel('F1 score')
    plt.title('Cross-validation {}'.format(name))
    plt.legend(bbox_to_anchor=(1, 1), loc='upper right', borderaxespad=0)
    plt.tight_layout()
    plt.savefig(dirname + 'ws_{}.png'.format(ws))
    plt.close(fig=fig)


def draw_all_prediction_results(all_results):
    """
    全体の結果を１つのグラフに

    ＊＊うまく動かないかも。。。＊＊

    Parameters
    -------------
    all_results: dict
        task, traitsのスコアと付随するダミースコアの入れ子辞書
        cf.     {
                'task': {'scores': <list>, 'stratified dummy': <list>, 'frequent dummy': <list>, 'feature importances': <list>, 'feature names': <list>},
                'Neuroticism': {'scores': <list>, 'stratified dummy': <list>, 'frequent dummy': <list>, 'feature importances': <list>, 'feature names': <list>},
                ...
                }
    """
    dirname = 'figures/all_results/'
    os.makedirs(dirname, exist_ok=True)
    names = ['task'] + conf.big5

    fig, ax = plt.subplots(figsize=(800, 600))
    for i, key in enumerate(all_results.keys()):
        ax.bar(
            np.arange(),
            np.mean(all_results[key]['scores']),
            yerr=np.std(all_results[key]['scores']),
            label='classifier(average: {:.4f})'.format(np.mean(all_results[key]['scores'])),
            align='center',
            color='orange',
            width=0.3
            )
        ax.bar(
            np.arange(),
            np.mean(all_results[key]['stratified dummy']),
            yerr=np.std(all_results[key]['stratified dummy']),
            label='stratified(average: {:.4f})'.format(np.mean(all_results[key]['stratified dummy'])),
            align='center',
            color='red',
            width=0.3
            )
        ax.bar(
            np.arange(),
            np.mean(all_results[key]['frequent dummy']),
            yerr=np.std(all_results[key]['frequent dummy']),
            label='most frequent(average: {:.4f})'.format(np.mean(all_results[key]['frequent dummy'])),
            align='center',
            color='blue',
            width=0.3
            )
    plt.xticks(np.arange(1.3, conf.outer_n_splits+1, 1.0), names)
    plt.ylabel('F1 score')
    plt.title('All')
    # plt.tight_layout()
    plt.savefig(dirname + 'ws_{}.png'.format(ws))
    plt.close(fig=fig)


def save_prediction(results_dict, name, ws):
    """
    csvファイルに各Cross Validationの予測結果を出力

    Parameters
    -------------------------------
    results_dict: dict
        クロスバリデーションの結果を格納した辞書
        cf.     {
                'scores': <list>, 'stratified dummy': <list>, 'frequent dummy': <list>, 'feature importances': <list>, 'feature names': <list>
                }
    name: str
        フォルダ名
    ws: int
        ウィンドウサイズ
    """
    dirname = 'figures/{}/'.format(name)
    os.makedirs(dirname, exist_ok=True)
    filename = dirname + 'ws_{}_cv_scores.csv'.format(ws)
    data = [['Fold'], np.arange(0, conf.outer_n_splits), ['Result'], results_dict['scores']]
    with open(filename, 'w', newline='') as f:  # csvファイルに書き込み
        writer = csv.writer(f)
        writer.writerows(data)


if __name__ == '__main__':

    for ws in conf.window_size:  # 5から135まで順に繰り返す
        print('\nws {}'.format(ws))
        all_results = dict()

        """ここから"""

        # preprocessingファイルの中のget_processed_eye_movement_data関数を呼び出す
        features, participant_id, task_labels, trait_labels, feature_columns = preprocessing.get_processed_eye_movement_data(ws)

        clf = RandomForestClassifier(n_jobs=-1, random_state=2019)

        # ここで予測を実行している
        task_results = predict_personality.prediction(
            features,
            task_labels,
            feature_columns,
            participant_id,
            'task', #文字列
            clf,
            conf.rfc_params,  # これが識別器の探索用パラメータ（conf.pyで編集可能)
            ws
        )

        draw_each_prediction_in_cv_result(task_results, ws, 'Task')  #グラフ描画
        draw_feature_importances(task_results, 'task', ws)  #グラフ描画
        save_prediction(task_results, 'Task', ws)  #予測結果保存

        all_results['task'] = task_results #すべての予測結果を辞書型変数に格納

        """ここまで タスクの予測"""

        """ここからぱーそなりてぃ"""
        for personality in conf.big5:

            trait_results = predict_personality.prediction(
                features,
                trait_labels.loc[:, personality],
                feature_columns,
                participant_id,
                personality,
                clf,
                conf.rfc_params,
                ws
            )
            draw_each_prediction_in_cv_result(trait_results, ws, personality)
            draw_feature_importances(trait_results, personality, ws)
            save_prediction(trait_results, personality, ws)
            all_results[personality] = trait_results

        draw_all_prediction_results(all_results)
