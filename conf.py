# --------------------feature names----------------------------------
big5 = ['Neuroticism', 'Extraversion', 'Openness', 'Agreeableness', 'Conscientiousness']

use_columns = [
    'RecordingTimestamp',
    'GazePointX (ADCSpx)',
    'GazePointY (ADCSpx)',
    'GazeEventType',
    'PupilLeft',
    'PupilRight'
]

# --------------------window size------------------------------------
window_size = [5, 15, 30, 45, 60, 75, 90, 105, 120, 135]

# --------------------classifier parameters---------------------------
outer_n_splits = 9  #データを９分割
inner_n_splits = 6  #９分割して１個取り出したセットをさらに６分割
outer_test_size = 0.7 #９分割して１個取り出したセットの中で、テストデータに割り当てるデータの割合　（学習データは３割）
inner_test_size = 0.5

rfc_params = { #ランダムフォレストのパラメタサーチ用のパラメタ
    'max_depth': [3, 5, 7, 9],
    'n_estimators': [100, 200, 300, 400, 500],
    'min_samples_split': [2, 3, 4, 5],
    'min_samples_leaf': [1, 2, 3, 4],
}
