import pandas as pd
import codecs
import os
import pprint
import sys
import traceback


class HandlingFiles:

    def __init__(self, directory_path):
        """directory_pathに移動する．絶対，相対両方OK"""

        self.df = self.read_func = None
        self.data_frame_list = []
        #self.columns = col_names
        if os.path.exists(directory_path):
            try:
                os.chdir(directory_path)
            except NotADirectoryError:
                raise Exception('ファイルパスを指定してください')
            self.content_names = os.listdir(os.getcwd())
            pprint.pprint("Directory: {0}\nContents: {1}".format(os.getcwd(), self.content_names))
        else:
            raise Exception('無効なパスです')

    def merge_or_concat(self, ext='csv', method=None):
        """ディレクトリ内にあるcsv(もしくはexcel)ファイル同士を全てくっつける．
        method='merge'なら行方向，method='concat'なら列方向"""
        data_file_names = None
        if ext == 'csv' or ext == 'xlsx':
            pass
        else:
            raise Exception('無効な拡張子です')

        if method == 'merge' or method == 'concat':
            if method == 'merge':
                if ext == 'csv':
                    data_file_names = [file for file in self.content_names if os.path.splitext(file)[1][1:] == 'csv']
                    if not data_file_names:
                        raise Exception('読み込み可能なファイルがありません')
                    self.read_func = pd.read_csv
                elif ext == 'xlsx':
                    data_file_names = [file for file in self.content_names if os.path.splitext(file)[1][1:] == 'xlsx']
                    if not data_file_names:
                        raise Exception('読み込み可能なファイルがありません')
                    self.read_func = pd.read_table
                for name in data_file_names:
                    #with codecs.open(name, 'r', 'utf-8', 'ignore') as file:
                    self.data_frame_list.append(self.read_func(name, encoding='utf-8', header=0))

                try:
                    self.df.merge(self.data_frame_list, axis=0, sort=True)
                    self.df = pd.concat(self.data_frame_list, axis=1, sort=True)
                except TypeError:
                    raise Exception('読み込めませんでした')

                return self.df

            else:
                if ext == 'csv':
                    data_file_names = [file for file in self.content_names if os.path.splitext(file)[1][1:] == 'csv']
                    if not data_file_names:
                        raise Exception('読み込み可能なファイルがありません')
                    self.read_func = pd.read_csv
                elif ext == 'xlsx':
                    data_file_names = [file for file in self.content_names if os.path.splitext(file)[1][1:] == 'xlsx']
                    if not data_file_names:
                        raise Exception('読み込み可能なファイルがありません')
                    self.read_func = pd.read_excel
                for name in data_file_names:
                    #with codecs.open(name, 'r', 'utf-8', 'ignore') as file:
                    self.data_frame_list.append(self.read_func(name, encoding='utf-8', header=0))

                try:
                    self.df = pd.concat(self.data_frame_list, axis=0, sort=True)
                except TypeError:
                    raise Exception('読み込めませんでした')

                return self.df

        else:
            raise Exception('methodには"merge"か"concat"を指定してください')

    def rename(self, batch=True):
        if batch:
            pass
        elif not batch:
            pass
        else:
            raise Exception('無効な引数です')
