import numpy as np
import os
from Handling import HandlingFiles as HF


def main():
    D_Y = 1080  # 画面解像度
    D_X = 1920

    df = HF('raw_gaze_data_all')  # データが入っているフォルダ名を指定
    df = df.merge_or_concat(ext='xlsx', method='concat')  # 指定したフォルダ内のエクセルファイルを全て読み込んでいる

    print(df.shape)

    #df.drop(conf.use_columns, axis=1, inplace=True)

    df = df[(df['RecordingTimestamp'] >= 60000) & (df['RecordingTimestamp'] <= 360000)]  # timestamp1分以上，6分以内のデータのみ使用する

    # x,y座標値が画面外にあるものをnanでパディング
    df.loc[:, 'GazePointX (ADCSpx)'][(df['GazePointX (ADCSpx)'] > D_X) | (df['GazePointX (ADCSpx)'] < 0)] = np.nan
    df.loc[:, 'GazePointY (ADCSpx)'][(df['GazePointY (ADCSpx)'] > D_Y) | (df['GazePointY (ADCSpx)'] < 0)] = np.nan

    # 座標を解像度で割って標準化
    df['GazePointX (ADCSpx)'] /= D_X
    df['GazePointY (ADCSpx)'] /= D_Y

    # GazeEventTypeのUnclassifiedをBlink文字列に置換
    df = df.replace({'GazeEventType': {'Unclassified': 'Blink'}})

    # Timestampの単位をsに統一
    df['RecordingTimestamp'] /= 1000.0

    # GazePointX, GazePointYおよびPupilLeft, PupilRightが片方欠損していれば互いのデータでパディング。両方nanの場合はそのまま
    df.loc[df['PupilLeft'].isnull(), 'PupilLeft'] = df.loc[df['PupilLeft'].isnull(), 'PupilRight']
    df.loc[df['PupilRight'].isnull(), 'PupilRight'] = df.loc[df['PupilRight'].isnull(), 'PupilLeft']
    #df['PupilLeft'].loc[df['PupilLeft'].isnull()] = df['PupilRight'][df['PupilLeft'].isnull()]
    #df['PupilRight'].loc[df['PupilRight'].isnull()] = df['PupilLeft'][df['PupilRight'].isnull()]

    # 列名を変更
    df = df.rename(columns={
        'GazePointX (ADCSpx)': 'x',
        'GazePointY (ADCSpx)': 'y',
        'RecordingTimestamp': 'time',
        'PupilLeft': 'left pupil diameter',
        'PupilRight': 'right pupil diameter',
        'GazeEventType': 'event strings'
        })


    # 個別のフォルダ，ファイル作成 Hoppeさん用の、個別のフォルダ、ファイルにする
    for t in df['RecordingName'].unique():  # F, I, E
        print('\n')
        os.makedirs('{}'.format(t), exist_ok=True)  # それぞれのタスクのフォルダを作成
        for i, p in enumerate(df['ParticipantName'].unique()):
            print('{} {}'.format(t, p))

            if p in ['P028']:  # 除外する被験者を指定
                continue

            os.makedirs('{}/{}'.format(t, p), exist_ok=True)  # 各被験者のフォルダを作成

            extracted_by_p = df[(df['ParticipantName'] == p) & (df['RecordingName'] == t)]

            # Timestampの値を0.0始まりに整形
            extracted_by_p.loc[:, 'time'] -= extracted_by_p['time'].iloc[0]

            # ファイルの書き出し
            # 欠損値は"nan"でパディング
            if not os.path.exists('{}/{}/events.csv'.format(t, p)):
                extracted_by_p.loc[:, 'event strings'].to_csv(
                    '{}/{}/events.csv'.format(t, p),
                    columns=['event strings'],
                    index=False,
                    header=True,
                    na_rep="nan"
                    )
            if not os.path.exists('{}/{}/gaze_positions.csv'.format(t, p)):
                extracted_by_p.loc[:, ['time', 'x', 'y']].to_csv(
                    '{}/{}/gaze_positions.csv'.format(t, p),
                    columns=['time', 'x', 'y'],
                    index=False,
                    header=True,
                    na_rep="nan",
                    )
            if not os.path.exists('{}/{}/pupil_diameter.csv'.format(t, p)):
                extracted_by_p.loc[:, ['time', 'right pupil diameter', 'left pupil diameter']].to_csv(
                    '{}/{}/pupil_diameter.csv'.format(t, p),
                    columns=['time', 'right pupil diameter', 'left pupil diameter'],
                    index=False,
                    header=True,
                    na_rep="nan"
                    )


if __name__ == '__main__':
    main()
