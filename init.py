import subprocess
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''\
                                         if you want to run all scripts automatically, pass '-a' or '--all' as a argument\
                                         ''')

    parser.add_argument(
        '-f',
        '--filename',
        help='pass the file names you want to run and each file needs to be separated by space',
        nargs='*'
    )
    parser.add_argument(
        '-a',
        '--all',
        help='run all scripts'
    )

    args = parser.parse_args()

    subprocess.run('python2 00_compute_')
