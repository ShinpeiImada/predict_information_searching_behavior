import numpy as np
import pandas as pd
import os
import argparse


def load_file():
    persona = pd.read_csv(
        'personality_traits/personality/personality_sex_age.csv',
        usecols=[
            'participant',
            'Age',
            'Neuroticism',
            'Extraversion',
            'Openness',
            'Agreeableness',
            'Conscientiousness']
    )  # 使用する列名

    persona = persona[persona['Age'] != 1]  # 除外処理
    persona.drop('Age', axis=1, inplace=True)

    return persona


def make_bins(df, num_category=3, cases=5):

    # 3値分類
    percent = 33
    for col in df.drop('participant', axis=1).columns:
        left_edge = np.percentile(df[col], percent)
        right_edge = np.percentile(df[col], percent * 2)
        df[col] = np.where(df[col] > right_edge, 3, np.where(df[col] < left_edge, 1, 2))
        df[col] = df[col].astype('float')

    # 2値分類
    """
    for col in df_c.drop('Participant ID', axis=1).columns:
        df_c[col] = np.where(df_c[col] >= thresholds[1], 2,  2)
        df_c[col] = df_c[col].astype('float')
    """
    # 4分類
    """
    for col in df_c.drop('Participant ID', axis=1).columns:
    df_c[col] = np.where(
        df_c[col] >= thresholds[2], 4, np.where(
            (df_c[col] < thresholds[2]) & (df_c[col] >= thresolds[1]), 3, np.where(
                df_c[col] < thresholds[0], 1, 2)
        )
    )
    df_c[col] = df_c[col].astype('float')
    """

    return df


if __name__ == '__main__':
    num_category = 3  # カテゴリ数
    cases = 7  # 回答項目数

    filepath = 'personality_traits/personality/personality_sex_age.csv'  # ファイルパス
    personality = load_file()
    df_personality = make_bins(personality, num_category, cases)

    df_personality.to_csv('personality_traits/personality/binned_personality.csv', index=False)  # 保存
