import numpy as np
import pandas as pd
import os
import csv
from sklearn.model_selection import GridSearchCV, GroupShuffleSplit
from sklearn.metrics import f1_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.dummy import DummyClassifier
from sklearn.preprocessing import StandardScaler
import conf


def prediction(X, Y, column_names, participant_id, target_name, clf, params, ws):
    """
    分類実行

    Parameters
    X: pandas.DataFrame
        特徴量
    traits: pandas.Series(or ndarray)
        目的変数（タスク分類でもこの変数名）
    column_names: pandas.Series(or ndarray)
        特徴量の列名
    participant_id: pandas.Series(or ndarray)
        被験者名（Xに合わせて引き伸ばした数）
    target_name: str
        目的変数名（タスクもしくはBIG５）
    clf: sklearn.classifier
        scikit-learn性のclassifier
    params: dict
        探索するパラメータセット
    ws: int
        ウィンドウサイズ
    """
    print("[{}]".format(target_name)) #推定している変数名を表示

    results = dict()
    scores = []
    dummy_str_scores = []
    dummy_freq_scores = []
    feature_importances = []
    best_estimators = []

    for K, (tr_idx, va_idx) in enumerate(GroupShuffleSplit(n_splits=conf.outer_n_splits, test_size=conf.outer_test_size, random_state=2019).split(X, Y, participant_id)): #同じparticipant_idをもつデータは切り離さない
        print('\tFold: {}'.format(K))
        train_x, test_x = X[tr_idx, :], X[va_idx, :]
        train_y, test_y = Y.iloc[tr_idx], Y.iloc[va_idx]
        train_id, test_id = participant_id.iloc[tr_idx], participant_id.iloc[va_idx]

        # スケール変換
        ss = StandardScaler().fit(train_x)
        train_x, test_x = ss.transform(train_x), ss.transform(test_x)

        gkf = list(GroupShuffleSplit( #インナーのパラメタサーチ用のフォールド：　group K hold
            n_splits=conf.inner_n_splits,
            test_size=conf.inner_test_size,
            random_state=2019
        ).split(train_x, train_y, train_id))

        dummy_stratified = DummyClassifier(random_state=2019)
        dummy_frequent = DummyClassifier(random_state=2019)
        dummy_stratified_params = {'strategy': ['stratified']}
        dummy_frequent_params = {'strategy': ['most_frequent']}

        # 内側クロスバリデーション（パラメータサーチ）
        grid = GridSearchCV(clf, param_grid=params, scoring='f1_micro', cv=gkf)  # conf.pyで探索するパラメータを設定している。
        dummy_str_grid = GridSearchCV(dummy_stratified, param_grid=dummy_stratified_params, scoring='f1_micro', cv=gkf)
        dummy_freq_grid = GridSearchCV(dummy_frequent, param_grid=dummy_frequent_params, scoring='f1_micro', cv=gkf)

        # Random Forestおよびダミー識別器の学習
        grid.fit(train_x, train_y) # 実際のパラメタサーチのルーチン
        dummy_str_grid.fit(train_x, train_y)
        dummy_freq_grid.fit(train_x, train_y)

        # 内側クロスバリデーションの内部状態を記録
        os.makedirs('results/cv_result/', exist_ok=True)
        pd.DataFrame(grid.cv_results_).to_csv('results/cv_result/{}_ws_{}_fold_{}.csv'.format(target_name, ws, K), index=False)

        # スコア(micro-F1)算出
        test_score = f1_score(test_y, grid.best_estimator_.predict(test_x), average='micro') # 最適なパラメタで回す
        str_dummy_score = f1_score(test_y, dummy_str_grid.best_estimator_.predict(test_x), average='micro')
        freq_dummy_score = f1_score(test_y, dummy_freq_grid.best_estimator_.predict(test_x), average='micro')

        # パラメータサーチの結果で一番良い識別器をリストに格納
        best_estimators.append(grid.best_estimator_)

        # 特徴量重要度
        feature_importances.append(grid.best_estimator_.feature_importances_)

        scores.append(test_score)
        dummy_str_scores.append(str_dummy_score)
        dummy_freq_scores.append(freq_dummy_score)

        print('\t\tScore: {}'.format(test_score))

    best_idx = np.argmax(scores)
    best_feature_importances = feature_importances[best_idx]

    results['scores'] = scores
    results['stratified dummy'] = dummy_str_scores
    results['frequent dummy'] = dummy_freq_scores
    results['feature importances'] = best_feature_importances
    results['feature names'] = column_names  # best_feature_names

    return results


def feature_selection(x, y, participant_id, n_features=30):
    """
    ランダムフォレストを使った特徴選択
    クロスバリデーションを行い、best_estimators_を使ってfeature_importances_属性を上からn_features分だけ選定した列インデックスを返す

    ＊＊実はこの関数は使っていない＊＊
    ＊＊Random Forest識別器はノイズに強い識別器なので、下手すると特徴選択が悪い結果をもたらす可能性があるため＊＊

    Parameters
    --------------
    x: pandas.DataFrame(or numpy.ndarray)
        すべての特徴量
    y: pandas.Series(or numpy.ndarray)
        目的変数
    participant_id: pandas.Series
        被験者ラベル
    n_features: int
        選択する特徴量の数

    Returns
    -------------
    important_index: List
        列インデックス
    """
    gkf = list(GroupShuffleSplit(
        n_splits=conf.inner_n_splits,
        test_size=conf.inner_test_size,
        random_state=2019
    ).split(x, y, participant_id))

    grid = GridSearchCV(
        RandomForestClassifier(n_jobs=-1),
        param_grid={'max_depth': [3, 4, 5], 'n_estimators': [100, 200, 300]},
        scoring='f1_macro',
        cv=gkf
    )
    grid.fit(x, y)

    important_index = [i for importance, i in sorted(zip(grid.best_estimator_.feature_importances_, range(grid.best_estimator_.n_features_)), key=lambda x: x[0], reverse=True)][:n_features]

    return important_index


def save_target_ratio(train_y, test_y, fold, target_name, ws):
    """
    各Fold内の目的変数の割合をcsvに保存

    ＊＊この関数も使用していない＊＊
    ＊＊デバッグ用に用意したやつだから＊＊

    Parameters
    ----------------------------------------
    train_y: pandas.Series
        トレーニングセットの目的変数
    test_y: pandas.Series
        テストセットの目的変数
    fold: int
        Fold番号
    target_name: str
        目的変数名
    ws: int
        ウィンドウサイズ

    Returns
    ----------------------------------------
    ratio: list
        各カテゴリの割合
    """
    dirname = 'results/target_ratio/'
    os.makedirs(dirname, exist_ok=True)
    filename = dirname + 'ratio_{}_ws_{}.csv'.format(target_name, ws)

    unique_values = sorted(train_y.unique())
    header = ['Fold num'] + ['class {} in training'.format(v) for v in unique_values]
    header += ['class {} in test'.format(v) for v in unique_values]
    header = [
        'Fold num',
        'class {} in training'.format(unique_values[0]),
        'class {} in training'.format(unique_values[1]),
        'class {} in training'.format(unique_values[2]),
        'class {} in test'.format(unique_values[0]),
        'class {} in test'.format(unique_values[1]),
        'class {} in test'.format(unique_values[2])
    ]

    if not os.path.exists(filename):
        with open(filename, 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(header)

    train_y_size = train_y.size
    test_y_size = test_y.size
    train_value_counts = train_y.value_counts().to_dict()
    test_value_counts = test_y.value_counts().to_dict()

    ratio = [
        fold,
        train_value_counts[unique_values[0]]/train_y_size,
        train_value_counts[unique_values[1]]/train_y_size,
        train_value_counts[unique_values[2]]/train_y_size,
        test_value_counts[unique_values[0]]/test_y_size,
        test_value_counts[unique_values[1]]/test_y_size,
        test_value_counts[unique_values[2]]/test_y_size
    ]

    with open(filename, 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(ratio)
