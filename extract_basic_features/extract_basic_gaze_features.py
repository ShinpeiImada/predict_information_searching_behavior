# using: utf-8
from collections import defaultdict
from concurrent import futures
from tqdm import tqdm
import numpy as np
import pandas as pd
import threading
import argparse
import glob
import os
import sys

def extract_gaze_features(args, df, results_dict):
	subject_name = df['ParticipantName'].iloc[0]  # 被験者名．別に0インデックスでなくてもよい
	project_name = df['StudioProjectName'].iloc[0]  # プロジェクト名
	if args.start:
		# 開始時間
		df = df.loc[df['RecordingTimestamp'] >= args.start, :]
	if args.end:
		# 終了時間
		df = df.loc[df['RecordingTimestamp'] <= args.end, :]

	fix_cnt, fix_dur_mean = detect_fixation(df)
	sacc_cnt, sacc_dur_mean = detect_saccade(df)
	pupil_size = detect_pupil(df)

	results_dict['StudioProjectName'].append(project_name)
	results_dict['Participant'].append(subject_name)
	results_dict['FixationCount'].append(fix_cnt)
	results_dict['AverageFixationDuration'].append(fix_dur_mean)
	results_dict['SaccadeCount'].append(sacc_cnt)
	results_dict['AverageSaccadeDuration'].append(sacc_dur_mean)
	results_dict['AveragePupil'].append(pupil_size)

def detect_fixation(df):
	# GazeEventTypeがFixationのサンプルのみを抽出
	fix_df = df.loc[df['GazeEventType'] == 'Fixation', :].copy()
	# 開始時間に依存しないよう，FixationCountを0始まりに
	fix_df.loc[:, 'FixationIndex'] = fix_df.loc[:, 'FixationIndex'] - fix_df['FixationIndex'].iloc[0] + 1

	# 一番最後のサンプルのFixationIndexをFixationCountとする
	fix_cnt = fix_df['FixationIndex'].iloc[-1].astype('int')

	if not fix_cnt:
		# If FixationIndex is empty, FixationDuration will be zero
		fix_dur_mean = 0
	else:
		# 同じFixationIndexを持つサンプルの最初のGazeEventDurationをFixation時間とみなし，それの平均をとる
		fix_dur_mean = np.mean(
			[fix_df.loc[fix_df['FixationIndex'] == index, :]['GazeEventDuration'].iloc[0] for index in range(1, fix_cnt+1)]
			)

	return fix_cnt, fix_dur_mean

def detect_saccade(df):  # ↑のやり方と一緒
	sacc_df = df.loc[df['GazeEventType'] == 'Saccade', :].copy()
	sacc_df.loc[:, 'SaccadeIndex'] = sacc_df.loc[:, 'SaccadeIndex'] - sacc_df['SaccadeIndex'].iloc[0] + 1

	sacc_cnt = sacc_df['SaccadeIndex'].iloc[-1].astype('int')
	if not sacc_cnt:
		sacc_dur_mean = 0
	else:
		sacc_dur_mean = np.mean(
			[sacc_df.loc[sacc_df['SaccadeIndex'] == index, :]['GazeEventDuration'].iloc[0] for index in range(1, sacc_cnt+1)]
			)

	return sacc_cnt, sacc_dur_mean

def detect_pupil(df):
	#  欠損値を無視したRight, Leftそれぞれの平均値を算出
	pupil_right = np.nanmean(df.loc[:, 'PupilRight'])
	pupil_left = np.nanmean(df.loc[:, 'PupilLeft'])

	# Right, Leftの平均値の平均値をPupilSizeとする
	average_both_pupil_size = np.mean([pupil_right, pupil_left])

	return average_both_pupil_size

def save_csv_after_sorting(filename):
	project_name = results_dict['StudioProjectName'][0]  # StudioProjectNameを入れる
	save_file = filename + '/' + 'created_basic_gaze_features_{}.csv'.format(project_name)

	# created_gaze_features.csvファイルが既に存在していれば存在しなくなるまでファイル名をインクリメントする
	count = 1
	while os.path.exists(save_file):
		save_file = 'created_basic_gaze_features_{} ({}).csv'.format(project_name, count)
		count += 1

	results_df = pd.DataFrame(results_dict)
	results_df.sort_values(by='Participant', ascending=True, inplace=True)
	results_df.reset_index(inplace=True, drop=True)

	results_df.to_csv(save_file, header=True, index=False)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="extract fix count, average fix and sacc duration and average pupil size from Tobii's file")
	parser.add_argument('filepath', help='directory path which data files exist')
	parser.add_argument('-a', '--all', help='if this argument are given, all files in the directory will be read. default is True', action='store_true')
	parser.add_argument('-i', '--individual', help="specify the file name. if '-a' or '--all' is passed, this argument will be ignored")
	parser.add_argument('-st', '--start', help='if you want to define start time, pass that time by milli second', type=int, default=None)
	parser.add_argument('-et', '--end' , help='if you want to define end time, pass that time by milli second', type=int, default=None)
	args = parser.parse_args()

	all_file_names = glob.glob(args.filepath + '/' + '*.xlsx')

	results_dict = defaultdict(list)
	print('{} Files'.format(len(all_file_names)))

	if not args.all and args.individual:
		individual_file = args.filepath + '/' + args.individual
		df = pd.read_excel(individual_file, header=0, usecols=[
		'ParticipantName',
		'StudioProjectName',
		'RecordingTimestamp',
		'GazeEventDuration',
		'GazeEventType',
		'FixationIndex',
		'SaccadeIndex',
		'PupilRight',
		'PupilLeft'
		])
		extract_gaze_features(args, df, results_dict)

	elif args.all:
		future_list = list()
		with futures.ThreadPoolExecutor(max_workers=4) as executor:
			for file in tqdm(all_file_names):
				df = pd.read_excel(file, header=0, usecols=[
				'ParticipantName',
				'StudioProjectName',
				'RecordingTimestamp',
				'GazeEventDuration',
				'GazeEventType',
				'FixationIndex',
				'SaccadeIndex',
				'PupilRight',
				'PupilLeft'
				])

				future = executor.submit(extract_gaze_features, args, df, results_dict)
				future_list.append(future)

		_ = futures.as_completed(fs=future_list)

	else:
		raise Exception('Pass the arguments correctly')
		sys.exit()

	save_csv_after_sorting(args.filepath)
